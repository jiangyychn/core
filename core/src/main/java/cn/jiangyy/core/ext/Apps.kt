package cn.jiangyy.core.ext

import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Build

val Context.appVersionCode: Long
    get() {
        return try {
            val packageInfo = applicationContext.packageManager
                .getPackageInfo(packageName, 0)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                packageInfo.longVersionCode
            } else {
                packageInfo.versionCode.toLong()
            }
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
            0
        }
    }


val Context.appVersionName: String
    get() {
        return try {
            val packageInfo = applicationContext
                .packageManager
                .getPackageInfo(packageName, 0)
            packageInfo.versionName.orEmpty()
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
            ""
        }
    }

val Context.isDarkMode: Boolean
    get() {
        val mode = resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK
        return mode == Configuration.UI_MODE_NIGHT_YES
    }