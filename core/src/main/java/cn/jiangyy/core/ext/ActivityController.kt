package cn.jiangyy.core.ext

import androidx.appcompat.app.AppCompatActivity

object ActivityController {

    private val mActivities = ArrayList<AppCompatActivity>()

    @JvmStatic
    fun addActivity(activity: AppCompatActivity) {
        mActivities.add(activity)
    }

    @JvmStatic
    fun removeActivity(activity: AppCompatActivity) {
        mActivities.remove(activity)
    }

    @JvmStatic
    fun finishAll() {
        for(activity in mActivities){
            if(!activity.isFinishing){
                activity.finish()
            }
        }
        mActivities.clear()
        mActivities.forEach {
            it.finish()
        }
    }

}