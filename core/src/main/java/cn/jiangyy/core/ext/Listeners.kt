package cn.jiangyy.core.ext

abstract class FastClickListener : android.view.View.OnClickListener {
    private var mLastClickTime: Long = 0
    private var interval = 1000L

    constructor()

    constructor(interval: Long) {
        this.interval = interval
    }

    override fun onClick(v: android.view.View) {
        val currentTime = System.currentTimeMillis()
        if (currentTime - mLastClickTime > interval) { // 经过了足够长的时间，允许点击
            onClick()
            mLastClickTime = getNowMills()
        }
    }

    protected abstract fun onClick()
}

inline fun android.view.View.click(noinline listener: (v: android.view.View) -> Unit) {
    this.setOnClickListener(object : FastClickListener() {
        override fun onClick() {
            hideSoftInput()
            listener(this@click)
        }
    })
}