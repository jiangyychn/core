package cn.jiangyy.core.ext

import android.util.Log
import java.io.File

private const val TAG = "FileExt"

fun File.ensureDir(): Boolean {
    try {
        if (!isDirectory) {
            if (isFile) {
                delete()
            }
            return mkdirs()
        }
    } catch (e: Exception) {
        Log.w(TAG, e.message.orEmpty())
    }
    return false
}