package cn.jiangyy.core.ext

import android.app.DatePickerDialog
import android.content.Context
import androidx.annotation.NonNull
import java.text.SimpleDateFormat
import java.util.*

/**
 * Return the current time in milliseconds.
 * @return [Long] the current time in milliseconds
 */
inline fun getNowMills(): Long = Calendar.getInstance().timeInMillis

/**
 * Return the current formatted time string.
 * @param pattern [String] The pattern of date format, default: yyyy-MM-dd HH:mm:ss
 * @return [String] the current formatted time string
 */
inline fun getNowString(pattern: String = "yyyy-MM-dd HH:mm:ss"): String =
    Calendar.getInstance().timeInMillis.millis2String(pattern)

/**
 * Return the current date.
 * @return [Date] the current date
 */
inline fun getNowDate(): Date = Date()

/**
 * Return the day of week in Chinese.
 * @receiver Date [Date] The date.
 * @return [String] the day of week in Chinese
 */
inline fun Date.getChineseWeek(): String = SimpleDateFormat("E", Locale.CHINA).format(this)

/**
 * Return the day of week in Chinese.
 * @receiver [String] The formatted time string.
 * @param pattern [String] The format.
 * @return [String] the day of week in Chinese
 */
inline fun String.getChineseWeek(pattern: String = "yyyy-MM-dd HH:mm:ss"): String =
    this.string2Date(pattern).getChineseWeek()

/**
 * Return the day of week in US.
 * @receiver [Long] The milliseconds.
 * @return [String] the day of week in Chinese
 */
inline fun Long.getUSWeek(): String = this.millis2Date().getChineseWeek()

/**
 * Return the day of week in US.
 * @receiver Date [Date] The date.
 * @return [String] the day of week in Chinese
 */
inline fun Date.getUSWeek(): String = SimpleDateFormat("EEEE", Locale.US).format(this)

/**
 * Return the day of week in US.
 * @receiver [String] The formatted time string.
 * @param pattern [String] The format.
 * @return [String] the day of week in Chinese
 */
inline fun String.getUSWeek(pattern: String = "yyyy-MM-dd HH:mm:ss"): String =
    this.string2Date(pattern).getChineseWeek()

/**
 * Return the day of week in Chinese.
 * @receiver [Long] The milliseconds.
 * @return [String] the day of week in Chinese
 */
inline fun Long.getChineseWeek(): String = this.millis2Date().getChineseWeek()

/**
 * Milliseconds to the formatted time string.
 * @receiver [Long] millis The milliseconds.
 * @param pattern [String] The pattern of date format, default: yyyy-MM-dd HH:mm:ss
 * @return [String] the formatted time string
 */
inline fun Long.millis2String(pattern: String = "yyyy-MM-dd HH:mm:ss"): String =
    SimpleDateFormat(pattern, Locale.getDefault()).format(this)

/**
 * Milliseconds to the date.
 * @receiver [Long] millis The milliseconds.
 * @return [Date] the date
 */
inline fun Long.millis2Date(): Date = Date(this)

/**
 * Formatted time string to the milliseconds.
 * @receiver [String] The formatted time string.
 * @param pattern [String] The pattern of date format, default: yyyy-MM-dd HH:mm:ss
 * @return [Long] the milliseconds
 */
inline fun String.string2Millis(pattern: String = "yyyy-MM-dd HH:mm:ss"): Long =
    SimpleDateFormat(pattern, Locale.getDefault()).parse(this).time

inline fun String.diaryDate(): String = this.string2Millis().millis2String("yyyy月MM月dd日")

/**
 * Formatted time string to the date.
 * @receiver [String] The formatted time string.
 * @param pattern [String] The format.
 * @return [Date] the date
 */
inline fun String.string2Date(pattern: String = "yyyy-MM-dd HH:mm:ss"): Date =
    SimpleDateFormat(pattern, Locale.getDefault()).parse(this)

/**
 * Milliseconds to the date.
 * @receiver [Date] the date
 * @return [Long] the milliseconds
 */
inline fun Date.date2Millis(): Long = this.time

/**
 * Date to the formatted time string.
 * @receiver [Date] The date.
 * @param pattern [String] The pattern of date format, default: yyyy-MM-dd HH:mm:ss
 * @return [String] the formatted time string
 */
inline fun Date.date2String(pattern: String = "yyyy-MM-dd HH:mm:ss"): String =
    SimpleDateFormat(pattern, Locale.getDefault()).format(this)


inline fun Context.getNowDateFormDialog(crossinline success: (String) -> Unit) {
    DatePickerDialog(
        this,
        { _, year, month, dayOfMonth ->
            success(
                "$year-${month + 1}-$dayOfMonth".string2Date("yyyy-MM-dd").date2String("yyyy-MM-dd")
            )
        },
        Calendar.getInstance().get(Calendar.YEAR),
        Calendar.getInstance().get(Calendar.MONTH),
        Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
    ).show()

}

object TimeSpan {
    const val DAY = 1000 * 60 * 60 * 24
    const val HOUR = 1000 * 60 * 60
    const val MINUTE = 1000 * 60
    const val SECOND = 1000
    const val MILLISECOND = 1
}

inline fun getTimeSpan(
    @NonNull startStr: String,
    @NonNull endStr: String,
    pattern: String = "yyyy-MM-dd",
    span: Int = TimeSpan.DAY

): Long {
    val startDate = SimpleDateFormat(pattern, Locale.getDefault()).parse(startStr)
    val endDate = SimpleDateFormat(pattern, Locale.getDefault()).parse(endStr)
    return (endDate.time - startDate.time) / span
}