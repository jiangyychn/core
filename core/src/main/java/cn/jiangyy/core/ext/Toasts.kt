package cn.jiangyy.core.ext

import android.content.Context
import android.widget.Toast
import androidx.fragment.app.Fragment

inline fun Context.toast(message: Int): Toast = Toast
    .makeText(this, "", Toast.LENGTH_SHORT)
    .apply {
        setText(message)
        show()
    }

inline fun Context.toast(message: CharSequence): Toast = Toast
    .makeText(this, "", Toast.LENGTH_SHORT)
    .apply {
        setText(message)
        show()
    }

inline fun Fragment.toast(message: Int) = requireActivity().toast(message)

inline fun Fragment.toast(message: CharSequence) = requireActivity().toast(message)

inline fun Context.longToast(message: Int): Toast = Toast
    .makeText(this, "", Toast.LENGTH_LONG)
    .apply {
        setText(message)
        show()
    }

inline fun Context.longToast(message: CharSequence): Toast = Toast
    .makeText(this, "", Toast.LENGTH_LONG)
    .apply {
        setText(message)
        show()
    }

inline fun Fragment.longToast(message: Int) = requireActivity().longToast(message)

inline fun Fragment.longToast(message: CharSequence) = requireActivity().longToast(message)