package cn.jiangyy.core.ext

import android.annotation.TargetApi
import android.app.Activity
import android.content.res.Resources
import android.os.Build
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import android.view.WindowManager
import androidx.annotation.ColorInt

@TargetApi(Build.VERSION_CODES.M)
inline fun Activity.setStatusBarColor(@ColorInt color: Int, lightMode: Boolean = false) {
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
    window.statusBarColor = color
    val decorView = window.decorView
    decorView.systemUiVisibility = if (lightMode) {
        decorView.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    } else {
        decorView.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
    }
}

///////////////////////////////////////////////////////////////////////////
// status bar
///////////////////////////////////////////////////////////////////////////
private const val TAG_STATUS_BAR = "TAG_STATUS_BAR"
private const val TAG_OFFSET = "TAG_OFFSET"
private const val KEY_OFFSET = -123

fun getStatusBarHeight(): Int {
    val resources = Resources.getSystem()
    val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
    return resources.getDimensionPixelSize(resourceId)
}

fun View.addMarginTopEqualStatusBarHeight() {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) return
    this.tag = TAG_OFFSET
    val haveSetOffset = this.getTag(KEY_OFFSET)
    if (haveSetOffset != null && haveSetOffset as Boolean) return
    val layoutParams = this.layoutParams as MarginLayoutParams
    layoutParams.setMargins(
        layoutParams.leftMargin,
        layoutParams.topMargin + getStatusBarHeight(),
        layoutParams.rightMargin,
        layoutParams.bottomMargin
    )
    this.setTag(KEY_OFFSET, true)
}

fun View.subtractMarginTopEqualStatusBarHeight() {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) return
    val haveSetOffset = this.getTag(KEY_OFFSET)
    if (haveSetOffset == null || !(haveSetOffset as Boolean)) return
    val layoutParams = this.layoutParams as MarginLayoutParams
    layoutParams.setMargins(
        layoutParams.leftMargin,
        layoutParams.topMargin - getStatusBarHeight(),
        layoutParams.rightMargin,
        layoutParams.bottomMargin
    )
    this.setTag(KEY_OFFSET, false)
}

inline val uiOS: UIOS
    get() = when (Build.BRAND) {
        "HUAWEI", "Huawei", "huawei", "HONOR", "Honor", "honor" -> UIOS.EMUI
        "XIAOMI", "Xiaomi", "xiaomi" -> UIOS.MIUI
        "OPPO", "Realme", "realme" -> UIOS.ColorOS
        "vivo", "iQOO" -> UIOS.FuntouchOS
        "Meizu" -> UIOS.Flyme
        "samsung" -> UIOS.OneUI
        "Meitu" -> UIOS.MEIOS
        "OnePlus" -> UIOS.H2OS
        "blackshark" -> UIOS.JOYUI
        "Nubia", "nubia" -> UIOS.NubiaUI
        "ZTE", "zte" -> UIOS.MiFavor
        else -> UIOS.Google
    }

inline val fullUIOS: UIOS
    get() = when (Build.BRAND) {
        "HUAWEI", "Huawei", "huawei", "HONOR", "Honor", "honor" -> UIOS.EMUI
        "XIAOMI", "Xiaomi", "xiaomi" -> UIOS.MIUI
        "OPPO", "Realme", "realme" -> UIOS.ColorOS
        "vivo", "iQOO" -> UIOS.FuntouchOS
        "Meizu" -> UIOS.Flyme
        "samsung" -> UIOS.OneUI
        "Meitu" -> UIOS.MEIOS
        "OnePlus" -> UIOS.H2OS
        "blackshark" -> UIOS.JOYUI
        "Nubia", "nubia" -> UIOS.NubiaUI
        "ZTE", "zte" -> UIOS.MiFavor
        "Lenovo", "ZUK", "zuk" -> UIOS.ZUI
        "GIONEE", "GiONEE" -> UIOS.AMIGO
        "SMARTISAN" -> UIOS.SmartisanOS
        "360" -> UIOS._360OS
        "LeEco", "Letv" -> UIOS.EUI
        "Nokia" -> UIOS.Nokia
        "HTC", "htc" -> UIOS.HTC
        "Sony", "sony" -> UIOS.SONY
        "Hisense",
        "S10_RJWN",
        "Z99",
        "DOOV", "doov" -> UIOS.Google
        else -> UIOS.Google
    }

/**
 * 市面上所有的主流手机品牌系统
 * - 三星：
 *    - Samsung - [OneUI]
 * - 华为
 *    - Huawei - [EMUI]
 *    - Honor - [EMUI]
 * - 小米
 *    - Xiaomi - [MIUI]
 *    - Redmi - [MIUI]
 *    - BlackShark - [JoyUI]
 * - 魅族
 *    - Meizu - [Flume]
 *    - Meilan - [Flume]
 * - OPPO
 *    - OPPO - [ColorOS]
 *    - Realme - [ColorOS]
 * - VIVO
 *    - VIVO - [FuntouchOS]
 *    - iQOO - [FuntouchOS]
 * - 一加
 *    - OnePlush - [H2OS]
 * - 努比亚
 *    - Nubia - [NubiaUI]
 * - 中兴
 *    - ZTE - [MiFavor]
 * - 联想
 *    - Lenovo - [ZUI]
 * - 美图
 *    - Meitu - [MEIOS]
 * - 金立
 *    - GIONEE - [AMIGO]
 * - 坚果
 *    - Smartisan - [SmartisanOS]
 * - 乐视
 *    - Le - [EUI]
 * - 360
 *    - 360 - [_360OS]
 * - 诺基亚
 *    - Nokia - [Nokia]，[Google]，好像是Android原生的
 * - HTC
 *    - HTC - [HTC]，[Google]，好像是Android原生的
 * - SONY
 *    - SONY - [SONY]，[Google]，好像是Android原生的
 */
enum class UIOS {

    /**
     * 小米、红米
     */
    MIUI,

    /**
     * 华为、荣耀
     */
    EMUI,

    /**
     * 魅族、魅蓝
     */
    Flyme,

    /**
     * OPPO，Realme
     */
    ColorOS,

    /**
     * VIVO
     */
    FuntouchOS,

    /**
     * 一加
     */
    H2OS,

    /**
     * 黑鲨
     */
    JOYUI,

    /**
     * 三星
     */
    OneUI,

    /**
     * Nubia
     */
    NubiaUI,

    /**
     * 美图
     */
    MEIOS,

    /**
     * 中兴
     */
    MiFavor,

    /**
     * 联想
     */
    @Deprecated("手机业务并入摩托罗拉")
    ZUI,

    /**
     * 金立
     */
    @Deprecated("2018年12月19日，金立正式宣布破产")
    AMIGO,

    /**
     * 坚果
     */
    @Deprecated("罗永浩退出锤子科技，手机业务暂停")
    SmartisanOS,

    /**
     * 乐视
     */
    @Deprecated("老板贾跃亭...唉，不说了，基本消失")
    EUI,

    /**
     * 360，手机业务暂停
     */
    @Deprecated("手机业务暂停")
    _360OS,

    /**
     * 诺基亚，经过长时间的斗争，Nokia还是败给了时代
     */
    @Deprecated("好像是Android原生，就不单独罗列了")
    Nokia,

    /**
     * 索尼
     */
    @Deprecated("好像是Android原生，就不单独罗列了")
    SONY,

    /**
     * HTC，昔日的只能手机霸主。把原Pixel手机设计研发团队卖给了谷歌，显示还是挣扎
     */
    @Deprecated("好像是Android原生，就不单独罗列了")
    HTC,

    /**
     * 海信、TCL、谷歌、朵唯、其他
     * - 可能基于Android原生系统进行了修改，不过由于市场占有量，均视为Android原生系统处理
     * - 此部分手机品牌会根据以后手机品牌的发展以进行动态修改
     */
    Google

}