package cn.jiangyy.core.ext

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.fragment.app.Fragment

/**
 * 打开默认浏览器
 * @receiver Context
 * @param url String
 */
inline fun Context.startBrowser(url: String) {
    val uri: Uri = Uri.parse(url)
    val intent = Intent(Intent.ACTION_VIEW, uri)
    startActivity(intent)
}

inline fun Fragment.startBrowser(url: String) = requireActivity().startBrowser(url)

/**
 * 跳转至拨号界面
 * @receiver Context
 * @param phoneNum String
 */
inline fun Context.callPhone(phoneNum: String) {
    val data = Uri.parse("tel:$phoneNum")
    val intent = Intent(Intent.ACTION_DIAL)
    intent.data = data
    startActivity(intent)
}

inline fun Fragment.callPhone(phoneNum: String) = requireActivity().callPhone(phoneNum)