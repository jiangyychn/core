package cn.jiangyy.core.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

abstract class BasicFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return initLayout(inflater, container, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initWidget()
        initData()
    }

    abstract fun initLayout(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View

    open fun initWidget() {

    }

    open fun initData() {

    }

}