package cn.jiangyy.core.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import cn.jiangyy.core.ext.ActivityController

abstract class BasicActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ActivityController.addActivity(this)
        initLayout()
        initWidget()
        initData()
    }

    abstract fun initLayout()

    open fun initWidget() {

    }

    open fun initData() {

    }

    override fun onDestroy() {
        super.onDestroy()
        ActivityController.removeActivity(this)
    }

}