package cn.jiangyy.core

import android.app.Application
import android.content.ContextWrapper
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.orhanobut.logger.PrettyFormatStrategy

private lateinit var INSTANCE: Application

object AppContext : ContextWrapper(INSTANCE)

abstract class CoreApp : Application() {

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
        PrettyFormatStrategy.newBuilder()
            .showThreadInfo(false)
            .methodCount(0)
            .tag("Core")
            .build()
            .apply {
                Logger.addLogAdapter(AndroidLogAdapter(this))
            }
    }


}