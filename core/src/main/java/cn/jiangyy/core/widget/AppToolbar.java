package cn.jiangyy.core.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.TintTypedArray;
import androidx.appcompat.widget.Toolbar;

import cn.jiangyy.core.R;

import static androidx.annotation.RestrictTo.Scope.TESTS;

/**
 * Custom {@link Toolbar} with center title
 *
 * @author JiangYY
 */
public class AppToolbar extends Toolbar {

    private TextView mCenterTitleTextView;
    private CharSequence mCenterTitleText;
    private ColorStateList mCenterTitleTextColor;

    public AppToolbar(Context context) {
        super(context);
        init(null, 0);
    }

    public AppToolbar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public AppToolbar(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    @SuppressLint("RestrictedApi")
    private void init(@Nullable AttributeSet attrs, int defStyle) {
        final TintTypedArray a = TintTypedArray.obtainStyledAttributes(getContext(), attrs,
                androidx.appcompat.R.styleable.Toolbar, defStyle, 0);
        final CharSequence title = a.getText(androidx.appcompat.R.styleable.Toolbar_title);
        if (!TextUtils.isEmpty(title)) {
            setTitle(title);
        }
        a.recycle();
    }

    public CharSequence getCenterTitle() {
        return mCenterTitleText;
    }

    @Override
    public void setTitle(CharSequence title) {
//        super.setTitle(title);
        if (!TextUtils.isEmpty(title)) {
            if (mCenterTitleTextView == null) {
                final Context context = getContext();
                mCenterTitleTextView = new AppCompatTextView(context);
                mCenterTitleTextView.setSingleLine();
                mCenterTitleTextView.setGravity(Gravity.CENTER);
                mCenterTitleTextView.setEllipsize(TextUtils.TruncateAt.END);
                setCenterTextAppearance(context, R.style.CoreTheme_Toolbar_Title);
                if (mCenterTitleTextColor != null) {
                    mCenterTitleTextView.setTextColor(mCenterTitleTextColor);
                }
            }
            if (!(mCenterTitleTextView.getParent() == this)) {
                addCenterView(mCenterTitleTextView);
            }
        } else if (mCenterTitleTextView != null && mCenterTitleTextView.getParent() == this) {
            removeView(mCenterTitleTextView);
        }
        if (mCenterTitleTextView != null) {
            mCenterTitleTextView.setText(title);
        }
        mCenterTitleText = title;

    }

    public void setCenterTextAppearance(Context context, int resId) {
        if (resId != 0) {
            mCenterTitleTextView.setTextAppearance(context, resId);
        }
    }

    @Override
    public void setTitleTextColor(@NonNull ColorStateList color) {
//        super.setTitleTextColor(color);
        mCenterTitleTextColor = color;
        if (mCenterTitleTextView != null) {
            mCenterTitleTextView.setTextColor(color);
        }
    }

    private void addCenterView(View v) {
        final ViewGroup.LayoutParams vlp = v.getLayoutParams();
        final LayoutParams lp;
        if (vlp == null) {
            lp = generateDefaultLayoutParams();
        } else if (!checkLayoutParams(vlp)) {
            lp = generateLayoutParams(vlp);
        } else {
            lp = (LayoutParams) vlp;
        }
        lp.gravity = Gravity.CENTER;
        addView(v, lp);
    }

    @RestrictTo(TESTS)
    @Nullable
    final TextView getCenterTitleTextView() {
        return mCenterTitleTextView;
    }

}
