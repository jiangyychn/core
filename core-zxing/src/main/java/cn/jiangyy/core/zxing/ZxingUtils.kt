package cn.jiangyy.core.zxing

import android.graphics.Bitmap
import android.graphics.Color
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.QRCodeWriter
import java.util.*

fun String.createZXing(width: Int = 300, height: Int = 300): Bitmap {
    val qrCodeWriter = QRCodeWriter()
    val hints: MutableMap<EncodeHintType, String> = EnumMap(EncodeHintType::class.java)
    hints[EncodeHintType.CHARACTER_SET] = "utf-8"
    var encode: BitMatrix? = null
    try {
        encode = qrCodeWriter.encode(this, BarcodeFormat.QR_CODE, width, height, hints)
    } catch (e: WriterException) {
        e.printStackTrace()
    }
    val colors = IntArray(width * height)
    for (i in 0 until width) {
        for (j in 0 until height) {
            if (encode!![i, j]) {
                colors[i * width + j] = Color.BLACK
            } else {
                colors[i * width + j] = Color.WHITE
            }
        }
    }
    return Bitmap.createBitmap(colors, width, height, Bitmap.Config.ARGB_8888)
}