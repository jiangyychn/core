# Core

[![](https://jitpack.io/v/com.gitee.jiangyychn/core.svg)](https://jitpack.io/#com.gitee.jiangyychn/core) &nbsp;
![api](https://img.shields.io/badge/API-21+-red) &nbsp;
![license](https://img.shields.io/badge/license-Apache--2.0-red) &nbsp;
![kotlin](https://img.shields.io/badge/kotlin-1.4.20-blue) &nbsp;

## [Wiki](https://github.com/jyygithub/arch/wikis)

## Gradle

Add the JitPack repository to your build file

```
allprojects {
    repositories {
        ...
        maven { url 'https://www.jitpack.io' }
    }
}
```

Add the dependency

```
dependencies {
    ...
    def core_version = "1.0.0-alpha01"
    implementation("com.gitee.jiangyychn.core:core:$core_version")
    implementation("com.gitee.jiangyychn.core:core-mvvm:$core_version")
}
```

## Libraries

0. [`Androidx`] + [`lifecycle`] + [`livedata`] + [`viewmodel`] + [`kotlin coroutines`]
1. [`Retrofit`](https://github.com/square/retrofit)
2. [`Retrofit-Gson`](https://github.com/square/retrofit/tree/master/retrofit-converters/gson)
3. [`PersistentCookieJar`](https://github.com/franmontiel/PersistentCookieJar)
4. [`OkHttp-Logger`](https://github.com/square/okhttp)
5. [`Logger`](https://github.com/orhanobut/logger)

## License

```
 Copyright 2020, JIANGYYCHN

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```