package cn.jiangyy.core_sample

import cn.jiangyy.core.base.BasicActivity
import cn.jiangyy.core.ext.click
import cn.jiangyy.core_sample.databinding.ActivityEntryBinding

class EntryActivity : BasicActivity() {

    private lateinit var mBinding: ActivityEntryBinding

    override fun initLayout() {
        mBinding = ActivityEntryBinding.inflate(layoutInflater)
    }

    override fun initWidget() {
        super.initWidget()
        mBinding.btnCoreZxing.click {
            ZXingActivity.actionStart(this)
        }
    }

    override fun initData() {
        super.initData()
    }
}