package cn.jiangyy.core_sample

import android.content.Context
import android.content.Intent
import cn.jiangyy.core.base.BasicActivity
import cn.jiangyy.core.ext.click
import cn.jiangyy.core.zxing.createZXing
import cn.jiangyy.core_sample.databinding.ActivityZxingBinding

class ZXingActivity : BasicActivity() {

    private lateinit var mBinding: ActivityZxingBinding

    override fun initLayout() {
        mBinding = ActivityZxingBinding.inflate(layoutInflater)
    }

    override fun initWidget() {
        super.initWidget()
        mBinding.btnCreate.click {
            mBinding.ivQRCode.setImageBitmap(
                "dsfsdsdf".createZXing()
            )
        }
    }


    companion object {
        fun actionStart(context: Context) {
            Intent(context, ZXingActivity::class.java).apply {
                context.startActivity(this)
            }
        }
    }

}