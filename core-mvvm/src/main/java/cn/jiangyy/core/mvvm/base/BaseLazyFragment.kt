package cn.jiangyy.core.mvvm.base

abstract class BaseLazyFragment : BaseFragment() {

    private var hasLoad = false

    override fun onResume() {
        super.onResume()
        if (!hasLoad) {
            initLazyData()
        }
    }

    open fun initLazyData() {
        hasLoad = true
    }

}