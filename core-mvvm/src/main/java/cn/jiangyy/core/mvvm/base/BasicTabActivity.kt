package cn.jiangyy.core.mvvm.base

import android.view.MenuItem
import androidx.fragment.app.Fragment
import cn.jiangyy.core.base.BasicActivity
import cn.jiangyy.core.mvvm.R
import cn.jiangyy.core.mvvm.ext.loadFragments
import cn.jiangyy.core.mvvm.ext.showHideFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

abstract class BasicTabActivity : BasicActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    override fun initLayout() {
        setContentView(R.layout.core_activity_basic_tab)
    }

    abstract val bottomMenu : Int

    abstract val fragmentsMap: Map<Int, Fragment>

    override fun initWidget() {
        super.initWidget()
        findViewById<BottomNavigationView>(R.id.core_bottomNavigationView).let {
            it.inflateMenu(bottomMenu)
            it.setOnNavigationItemSelectedListener(this)
        }
        loadFragments(R.id.core_frameLayout, 0, *fragmentsMap.values.toTypedArray())
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        showHideFragment(fragmentsMap.getValue(p0.itemId))
        return true
    }

}