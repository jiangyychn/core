package cn.jiangyy.core.mvvm.base

import cn.jiangyy.core.base.BasicActivity
import com.lxj.xpopup.XPopup
import com.lxj.xpopup.animator.EmptyAnimator

/**
 * Inflater layout using setContentView(layoutRes)
 */
abstract class BaseActivity : BasicActivity() {

    private val mLoadingDialog by lazy {
        XPopup.Builder(this)
            .dismissOnBackPressed(false)
            .dismissOnTouchOutside(false)
            .customAnimator(EmptyAnimator(null))
            .asLoading()
    }

    abstract val layoutRes: Int

    override fun initLayout() {
        setContentView(layoutRes)
    }

    final fun showLoadingDialog(text: String) {
        mLoadingDialog.setTitle(text).show()
    }

    final fun dismissLoadingDialog() {
        mLoadingDialog.dismiss()
    }

}