package cn.jiangyy.core.mvvm.ext

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

val mGson = GsonBuilder().registerTypeAdapterFactory(GsonTypeAdapterFactory()).create()

inline fun <reified T> String.gsonObject(): T = mGson.fromJson(this, T::class.java)

inline fun Any?.gsonString(): String = if (this == null) "" else mGson.toJson(this)

inline fun <reified T> String.gsonList(): MutableList<T> {
    val typeOf = object : TypeToken<MutableList<String>>() {}.type
    return Gson().fromJson(this, typeOf)
}