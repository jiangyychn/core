package cn.jiangyy.core.mvvm.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.jiangyy.core.base.BasicFragment
import com.lxj.xpopup.XPopup
import com.lxj.xpopup.animator.EmptyAnimator

abstract class BaseFragment : BasicFragment() {

    private val mLoadingDialog by lazy {
        XPopup.Builder(activity)
            .dismissOnBackPressed(false)
            .dismissOnTouchOutside(false)
            .customAnimator(EmptyAnimator(null))
            .asLoading()
    }

    abstract val layoutRes: Int

    override fun initLayout(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(layoutRes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initWidget()
        initData()
    }

    final fun showLoadingDialog(text: String) {
        mLoadingDialog.setTitle(text).show()
    }

    final fun dismissLoadingDialog() {
        mLoadingDialog.dismiss()
    }

}