package cn.jiangyy.core.mvvm.ext

import androidx.lifecycle.liveData
import kotlinx.coroutines.Dispatchers

fun <T> fire(block: suspend () -> Result<T>) =
    liveData<Result<T>>(Dispatchers.IO) {
        val result = try {
            block()
        } catch (e: Exception) {
            Result.failure<T>(e)
        }
        emit(result)
    }