package cn.jiangyy.core.mvvm.utils

import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.preferencesKey
import androidx.datastore.preferences.createDataStore
import cn.jiangyy.core.AppContext
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking

object DataStoreUtils {

    private val dataStore by lazy { AppContext.createDataStore(AppContext.packageName) }

    fun putValue(key: String, value: Int) {
        val _key = preferencesKey<Int>(key)
        runBlocking {
            dataStore.edit { settings ->
                settings[_key] = value
            }
        }
    }

    fun getValue(key: String, default: Int): Int {
        val _key = preferencesKey<Int>(key)
        return runBlocking {
            dataStore.data.map { settings ->
                settings[_key] ?: default
            }.first()
        }
    }

    fun putValue(key: String, value: Long) {
        val _key = preferencesKey<Long>(key)
        runBlocking {
            dataStore.edit { settings ->
                settings[_key] = value
            }
        }
    }

    fun getValue(key: String, default: Long): Long {
        val _key = preferencesKey<Long>(key)
        return runBlocking {
            dataStore.data.map { settings ->
                settings[_key] ?: default
            }.first()
        }
    }

    fun putValue(key: String, value: String) {
        val _key = preferencesKey<String>(key)
        runBlocking {
            dataStore.edit { settings ->
                settings[_key] = value
            }
        }
    }

    fun getValue(key: String, default: String): String {
        val _key = preferencesKey<String>(key)
        return runBlocking {
            dataStore.data.map { settings ->
                settings[_key] ?: default
            }.first()
        }
    }

    fun putValue(key: String, value: Boolean) {
        val _key = preferencesKey<Boolean>(key)
        runBlocking {
            dataStore.edit { settings ->
                settings[_key] = value
            }
        }
    }

    fun getValue(key: String, default: Boolean): Boolean {
        val _key = preferencesKey<Boolean>(key)
        return runBlocking {
            dataStore.data.map { settings ->
                settings[_key] ?: default
            }.first()
        }
    }

    fun putValue(key: String, value: Float) {
        val _key = preferencesKey<Float>(key)
        runBlocking {
            dataStore.edit { settings ->
                settings[_key] = value
            }
        }
    }

    fun getValue(key: String, default: Float): Float {
        val _key = preferencesKey<Float>(key)
        return runBlocking {
            dataStore.data.map { settings ->
                settings[_key] ?: default
            }.first()
        }
    }

    fun putValue(key: String, value: Double) {
        val _key = preferencesKey<Double>(key)
        runBlocking {
            dataStore.edit { settings ->
                settings[_key] = value
            }
        }
    }

    fun getValue(key: String, default: Double): Double {
        val _key = preferencesKey<Double>(key)
        return runBlocking {
            dataStore.data.map { settings ->
                settings[_key] ?: default
            }.first()
        }
    }

}