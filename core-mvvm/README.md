Add the JitPack repository to your build file

```
allprojects {
    repositories {
        ...
        maven { url 'https://www.jitpack.io' }
    }
}
```

Add the dependency

```
dependencies {
    ...
    def core_version = "0.2.4"
    implementation("com.gitee.jiangyychn.core:core-mvvm:$core_version")
}
```