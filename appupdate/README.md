## Gradle

Add the JitPack repository to your build file

```
allprojects {
    repositories {
        ...
        maven { url 'https://www.jitpack.io' }
    }
}
```

Add the dependency

```
dependencies {
    ...
    def core_version = "0.1.4"
    implementation("com.gitee.jiangyychn.core:core:$core_version")
}
```

## Usage

### Base

```kotlin
class MyActivity : BaseActivity() {

}
class MyFragment : BaseFragment() {

}
class MyApp : CoreApp() {

}
```

### Ext

1. Calendars
```kotlin
getNowMills()
getNowDate()
getNowDate()
```
2. Dimensions
```kotlin
val value = 0.5f.dp
```
3. InputMethods
```kotlin
toggleSoftInput()
showSoftInput()
hideSoftInput()
```
4. ~~Internals~~
```kotlin
startActivity<MainActivity>()
startActivity<MainActivity>("param" to "value")
startBrowser("https://www.google.com/")
callPhone("10000")
```
5. Toasts
```kotlin
toast("success")
longToast("failure")
```
6. Listeners
```kotlin
textViews.click {
    // TODO
}
```
7. Prefs
```kotlin
val isLogin by pref(false)
```
8. Views
```kotlin
recyclerView.linearLayoutManager(RecyclerView.HORIZONTAL).adapter = myAdapter
recyclerView.gridLayoutManager(8).adapter = myAdapter
toolbar.finish()
```
9. Datas
```kotlin
boolean?.orDefault()
bouble?.orZero()
string?.orDefault()
```
10. Apps
```kotlin
val versionCode = appVersionCode
val versioName = appVersionName
val boolean = isDarkMode
```

### Widget

```kotlin
cn.jiangyy.core.widget.AppToolbar
cn.jiangyy.core.widget.LoadingDialog
```